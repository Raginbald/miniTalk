# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: rthebaud <rthebaud@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/01/09 10:34:35 by rthebaud          #+#    #+#              #
#    Updated: 2014/02/08 09:26:45 by rthebaud         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

PROJ_1=client
PROJ_2=serveur

all:
	$(MAKE) -C libft
	$(MAKE) -C $(PROJ_1)
	$(MAKE) -C $(PROJ_2)

clean:
	$(MAKE) clean -C libft
	$(MAKE) clean -C $(PROJ_1)
	$(MAKE) clean -C $(PROJ_2)

fclean: clean
	$(MAKE) fclean -C libft
	$(MAKE) fclean -C $(PROJ_1)
	$(MAKE) fclean -C $(PROJ_2)

re: fclean all
