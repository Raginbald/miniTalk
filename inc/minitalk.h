/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minitalk.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rthebaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/08 09:15:31 by rthebaud          #+#    #+#             */
/*   Updated: 2014/02/09 19:35:34 by rthebaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __MINITALK_H__
# define __MINITALK_H__

# include <unistd.h>
# include <signal.h>
# include <libft.h>
# include <ft_printf.h>

# define CHAR_SIZE 7
# define INT_SIZE 31

typedef struct s_sig	t_sig;

enum	e_flg
{
	F_NONE = 0,
	F_PID = 1,
	F_STRLEN = 2,
	F_STR = 4,
};

struct	s_sig
{
	char			*str;
	unsigned int	i;
	int				j;
	int				flags;
	int				pid;
	int				len;
};

#endif /* !__MINITALK_H__ */
