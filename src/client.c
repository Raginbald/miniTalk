/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rthebaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/08 08:55:25 by rthebaud          #+#    #+#             */
/*   Updated: 2014/02/22 16:50:21 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minitalk.h"

static void	_send_char(int pid, char c)
{
	int		i;

	i = 1 << CHAR_SIZE;
	while (i)
	{
		if ((c & i) == i)
			kill(pid, SIGUSR1);
		else
			kill(pid, SIGUSR2);
		pause();
		i = i >> 1;
	}
}

static void	_send_str(int pid, char *str)
{
	while (*str)
	{
		_send_char(pid, *str);
		str++;
	}
}

static void	_send_int(int pid, int n, int p)
{
	unsigned int		i;

	i = 1 << INT_SIZE;
	while (i)
	{
		if ((n & i) == i)
			kill(pid, SIGUSR1);
		else
			kill(pid, SIGUSR2);
		if (p)
			pause();
		else
			usleep(500);
		i = i >> 1;
	}
}

static void	_sig(int sig)
{
	sig = sig;
}

int			main(int argc, const char *argv[])
{
	int	pid;

	if (argc != 3)
	{
		ft_printf("usage: %s server_pid string\n", argv[0]);
		return (1);
	}
	signal(SIGUSR1, _sig);
	pid = ft_atoi(argv[1]);
	_send_int(pid, getpid(), 0);
	_send_int(pid, ft_strlen(argv[2]), 1);
	_send_str(pid, (char *)argv[2]);
	return (0);
}
