/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   serveur.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rthebaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/08 08:56:21 by rthebaud          #+#    #+#             */
/*   Updated: 2014/02/19 16:36:28 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minitalk.h"

static int	_pid(int sig, t_sig *s)
{
	if (sig== SIGUSR1)
		s->pid |= s->i;
	s->i = s->i >> 1;
	if (s->i == 0)
	{
		s->flags |= F_PID;
		s->i = 1 << INT_SIZE;
	}
	return (1);
}

static int	_len(int sig, t_sig *s)
{
	if (sig== SIGUSR1)
		s->len |= s->i;
	s->i = s->i >> 1;
	if (s->i == 0)
	{
		s->flags |= F_STRLEN;
		s->str = (char *)ft_strnew(s->len);
		s->i = 1 << CHAR_SIZE;
	}
	kill(s->pid, SIGUSR1);
	return (1);
}

static int	_char(int sig, t_sig *s)
{
	int	pid;

	pid = s->pid;
	if (sig == SIGUSR1)
		s->str[s->j] |= s->i;
	s->i = s->i >> 1;
	if (s->i == 0)
	{
		s->i = 1 << CHAR_SIZE;
		s->j++;
		if (s->j >= s->len)
		{
			ft_putendl(s->str);
			ft_strdel(&s->str);
			s->i = 1 << INT_SIZE;
			s->flags |= F_STR;
			s->pid = 0;
			s->j = 0;
			s->len = 0;
			s->flags = F_NONE;
		}
	}
	kill(pid, SIGUSR1);
	return (1);
}

static void	_get(int sig)
{
	static t_sig	s = { NULL, 1 << INT_SIZE, 0, 0, 0, 0 };
	int				ok;

	ok = 0;
	ok = ok || (!(s.flags & F_PID) && _pid(sig, &s));
	ok = ok || (!(s.flags & F_STRLEN) && _len(sig, &s));
	ok = ok || (!(s.flags & F_STR) && _char(sig, &s));
}

int			main(void)
{
	ft_printf("%d\n", getpid());
	signal(SIGUSR1, _get);
	signal(SIGUSR2, _get);
	while (1)
		pause();
	return (0);
}
